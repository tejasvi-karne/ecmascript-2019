<!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="../css/ecmarkup.css" rel="stylesheet">
    <title>intro</title>
  </head>
  <body>
    <emu-intro id="sec-intro">
  <h1>Introduction</h1>
  <p>This Ecma Standard defines the ECMAScript 2018 Language. It is the
tenth edition of the ECMAScript Language Specification. Since
publication of the first edition in 1997, ECMAScript has grown to be one
 of the world's most widely used general-purpose programming languages.
It is best known as the language embedded in web browsers but has also
been widely adopted for server and embedded applications.</p>
  <p>ECMAScript is based on several originating technologies, the most
well-known being JavaScript (Netscape) and JScript (Microsoft). The
language was invented by Brendan Eich at Netscape and first appeared in
that company's Navigator 2.0 browser. It has appeared in all subsequent
browsers from Netscape and in all browsers from Microsoft starting with
Internet Explorer 3.0.</p>
  <p>The development of the ECMAScript Language Specification started in
 November 1996. The first edition of this Ecma Standard was adopted by
the Ecma General Assembly of June 1997.</p>
  <p>That Ecma Standard was submitted to ISO/IEC JTC 1 for adoption
under the fast-track procedure, and approved as international standard
ISO/IEC 16262, in April 1998. The Ecma General Assembly of June 1998
approved the second edition of ECMA-262 to keep it fully aligned with
ISO/IEC 16262. Changes between the first and the second edition are
editorial in nature.</p>
  <p>The third edition of the Standard introduced powerful regular
expressions, better string handling, new control statements, try/catch
exception handling, tighter definition of errors, formatting for numeric
 output and minor changes in anticipation of future language growth. The
 third edition of the ECMAScript standard was adopted by the Ecma
General Assembly of December 1999 and published as ISO/IEC 16262:2002 in
 June 2002.</p>
  <p>After publication of the third edition, ECMAScript achieved massive
 adoption in conjunction with the World Wide Web where it has become the
 programming language that is supported by essentially all web browsers.
 Significant work was done to develop a fourth edition of ECMAScript.
However, that work was not completed and not published as the fourth
edition of ECMAScript but some of it was incorporated into the
development of the sixth edition.</p>
  <p>The fifth edition of ECMAScript (published as ECMA-262 5<sup>th</sup>
 edition) codified de facto interpretations of the language
specification that have become common among browser implementations and
added support for new features that had emerged since the publication of
 the third edition. Such features include accessor properties,
reflective creation and inspection of objects, program control of
property attributes, additional array manipulation functions, support
for the JSON object encoding format, and a strict mode that provides
enhanced error checking and program security. The fifth edition was
adopted by the Ecma General Assembly of December 2009.</p>
  <p>The fifth edition was submitted to ISO/IEC JTC 1 for adoption under
 the fast-track procedure, and approved as international standard
ISO/IEC 16262:2011. Edition 5.1 of the ECMAScript Standard incorporated
minor corrections and is the same text as ISO/IEC 16262:2011. The 5.1
Edition was adopted by the Ecma General Assembly of June 2011.</p>
  <p>Focused development of the sixth edition started in 2009, as the
fifth edition was being prepared for publication. However, this was
preceded by significant experimentation and language enhancement design
efforts dating to the publication of the third edition in 1999. In a
very real sense, the completion of the sixth edition is the culmination
of a fifteen year effort. The goals for this addition included providing
 better support for large applications, library creation, and for use of
 ECMAScript as a compilation target for other languages. Some of its
major enhancements included modules, class declarations, lexical block
scoping, iterators and generators, promises for asynchronous
programming, destructuring patterns, and proper tail calls. The
ECMAScript library of built-ins was expanded to support additional data
abstractions including maps, sets, and arrays of binary numeric values
as well as additional support for Unicode supplemental characters in
strings and regular expressions. The built-ins were also made extensible
 via subclassing. The sixth edition provides the foundation for regular,
 incremental language and library enhancements. The sixth edition was
adopted by the General Assembly of June 2015.</p>
  <p>ECMAScript 2016 was the first ECMAScript edition released under
Ecma TC39's new yearly release cadence and open development process. A
plain-text source document was built from the ECMAScript 2015 source
document to serve as the base for further development entirely on
GitHub. Over the year of this standard's development, hundreds of pull
requests and issues were filed representing thousands of bug fixes,
editorial fixes and other improvements. Additionally, numerous software
tools were developed to aid in this effort including Ecmarkup,
Ecmarkdown, and Grammarkdown. ES2016 also included support for a new
exponentiation operator and adds a new method to Array.prototype called <code>includes</code>.</p>
  <p>ECMAScript 2017 introduced Async Functions, Shared Memory, and
Atomics along with smaller language and library enhancements, bug fixes,
 and editorial updates. Async functions improve the asynchronous
programming experience by providing syntax for promise-returning
functions. Shared Memory and Atomics introduce a new <emu-xref href="#sec-memory-model" id="_ref_834"><a href="#sec-memory-model">memory model</a></emu-xref> that allows multi-<emu-xref href="#agent" id="_ref_835"><a href="#agent">agent</a></emu-xref>
 programs to communicate using atomic operations that ensure a
well-defined execution order even on parallel CPUs. This specification
also includes new static methods on Object: <code>Object.values</code>, <code>Object.entries</code>, and <code>Object.getOwnPropertyDescriptors</code>.</p>
  <p>This specification, the 9<sup>th</sup> edition, introduces support
for asynchronous iteration via the AsyncIterator protocol and async
generators. This specification also includes four new regular expression
 features: the dotAll flag, named capture groups, Unicode property
escapes, and look-behind assertions. It also includes rest parameter and
 spread operator support for object properties. There have also been
many minor updates, editorial and normative, with many contributions
from our awesome community.</p>
  <p></p>
  <p>Dozens of individuals representing many organizations have made
very significant contributions within Ecma TC39 to the development of
this edition and to the prior editions. In addition, a vibrant community
 has emerged supporting TC39's ECMAScript efforts. This community has
reviewed numerous drafts, filed thousands of bug reports, performed
implementation experiments, contributed test suites, and educated the
world-wide developer community about ECMAScript. Unfortunately, it is
impossible to identify and acknowledge every person and organization who
 has contributed to this effort.</p>
  <p>
    Allen Wirfs-Brock<br>
    ECMA-262, 6<sup>th</sup> Edition Project Editor

  </p>
  <p>
    Brian Terlson<br>
    ECMA-262, 7<sup>th</sup> Edition Project Editor

  </p>
</emu-intro>

    <nav class="bottom-nav">
      <ul>
        <li><a href="undefined.html">Previous</a></li>
        <li><a href="toc.html">TOC</a></li>
        <li><a href="scope.html">Next</a></li>
      </ul>
    </nav>
    <script src="../js/script.js"></script>
  </body>
  </html>