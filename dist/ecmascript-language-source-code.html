<!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="../css/ecmarkup.css" rel="stylesheet">
    <title>ecmascript-language-source-code</title>
  </head>
  <body>
    <emu-clause id="sec-ecmascript-language-source-code">
  <h1><span class="secnum">10</span>ECMAScript Language: Source Code</h1>

  <emu-clause id="sec-source-text">
    <h1><span class="secnum">10.1</span>Source Text</h1>
    <h2>Syntax</h2>
    <emu-grammar type="definition"><emu-production name="SourceCharacter" type="lexical" id="prod-SourceCharacter">
    <emu-nt><a href="#prod-SourceCharacter">SourceCharacter</a></emu-nt><emu-geq>::</emu-geq><emu-rhs a="c64b38bd"><emu-gprose>any Unicode code point</emu-gprose></emu-rhs>
</emu-production></emu-grammar>
    <p>ECMAScript code is expressed using Unicode. ECMAScript source 
text is a sequence of code points. All Unicode code point values from 
U+0000 to U+10FFFF, including surrogate code points, may occur in source
 text where permitted by the ECMAScript grammars. The actual encodings 
used to store and interchange ECMAScript source text is not relevant to 
this specification. Regardless of the external source text encoding, a 
conforming ECMAScript implementation processes the source text as if it 
was an equivalent sequence of <emu-nt id="_ref_9095"><a href="#prod-SourceCharacter">SourceCharacter</a></emu-nt> values, each <emu-nt id="_ref_9096"><a href="#prod-SourceCharacter">SourceCharacter</a></emu-nt>
 being a Unicode code point. Conforming ECMAScript implementations are 
not required to perform any normalization of source text, or behave as 
though they were performing normalization of source text.</p>
    <p>The components of a combining character sequence are treated as 
individual Unicode code points even though a user might think of the 
whole sequence as a single character.</p>
    <emu-note><span class="note">Note</span><div class="note-contents">
      <p>In string literals, regular expression literals, template 
literals and identifiers, any Unicode code point may also be expressed 
using Unicode escape sequences that explicitly express a code point's 
numeric value. Within a comment, such an escape sequence is effectively 
ignored as part of the comment.</p>
      <p>ECMAScript differs from the Java programming language in the 
behaviour of Unicode escape sequences. In a Java program, if the Unicode
 escape sequence <code>\u000A</code>, for example, occurs within a 
single-line comment, it is interpreted as a line terminator (Unicode 
code point U+000A is LINE FEED (LF)) and therefore the next code point 
is not part of the comment. Similarly, if the Unicode escape sequence <code>\u000A</code>
 occurs within a string literal in a Java program, it is likewise 
interpreted as a line terminator, which is not allowed within a string 
literal—one must write <code>\n</code> instead of <code>\u000A</code> to
 cause a LINE FEED (LF) to be part of the String value of a string 
literal. In an ECMAScript program, a Unicode escape sequence occurring 
within a comment is never interpreted and therefore cannot contribute to
 termination of the comment. Similarly, a Unicode escape sequence 
occurring within a string literal in an ECMAScript program always 
contributes to the literal and is never interpreted as a line terminator
 or as a code point that might terminate the string literal.</p>
    </div></emu-note>

    <emu-clause id="sec-utf16encoding" aoid="UTF16Encoding">
      <h1><span class="secnum">10.1.1</span>Static Semantics: UTF16Encoding ( <var>cp</var> )</h1>
      <p>The UTF16Encoding of a numeric code point value, <var>cp</var>, is determined as follows:</p>
      <emu-alg><ol><li><emu-xref href="#assert" id="_ref_3095"><a href="#assert">Assert</a></emu-xref>: 0 ≤ <var>cp</var> ≤ 0x10FFFF.</li><li>If <var>cp</var> ≤ 0xFFFF, return <var>cp</var>.</li><li>Let <var>cu1</var> be <emu-xref aoid="floor" id="_ref_3096"><a href="#eqn-floor">floor</a></emu-xref>((<var>cp</var> - 0x10000) / 0x400) + 0xD800.</li><li>Let <var>cu2</var> be ((<var>cp</var> - 0x10000) <emu-xref aoid="modulo" id="_ref_3097"><a href="#eqn-modulo">modulo</a></emu-xref> 0x400) + 0xDC00.</li><li>Return the code unit sequence consisting of <var>cu1</var> followed by <var>cu2</var>.
      </li></ol></emu-alg>
    </emu-clause>

    <emu-clause id="sec-utf16decode" aoid="UTF16Decode">
      <h1><span class="secnum">10.1.2</span>Static Semantics: UTF16Decode ( <var>lead</var>, <var>trail</var> )</h1>
      <p>Two code units, <var>lead</var> and <var>trail</var>, that form a UTF-16  <emu-xref href="#surrogate-pair" id="_ref_251"><a href="#surrogate-pair">surrogate pair</a></emu-xref> are converted to a code point by performing the following steps:</p>
      <emu-alg><ol><li><emu-xref href="#assert" id="_ref_3098"><a href="#assert">Assert</a></emu-xref>: <var>lead</var> is a <emu-xref href="#leading-surrogate" id="_ref_252"><a href="#leading-surrogate">leading surrogate</a></emu-xref> and <var>trail</var> is a <emu-xref href="#trailing-surrogate" id="_ref_253"><a href="#trailing-surrogate">trailing surrogate</a></emu-xref>.</li><li>Let <var>cp</var> be (<var>lead</var> - 0xD800) × 0x400 + (<var>trail</var> - 0xDC00) + 0x10000.</li><li>Return the code point <var>cp</var>.
      </li></ol></emu-alg>
    </emu-clause>
  </emu-clause>

  <emu-clause id="sec-types-of-source-code">
    <h1><span class="secnum">10.2</span>Types of Source Code</h1>
    <p>There are four types of ECMAScript code:</p>
    <ul>
      <li>
        <em>Global code</em> is source text that is treated as an ECMAScript <emu-nt id="_ref_9097"><a href="#prod-Script">Script</a></emu-nt>. The global code of a particular <emu-nt id="_ref_9098"><a href="#prod-Script">Script</a></emu-nt> does not include any source text that is parsed as part of a <emu-nt id="_ref_9099"><a href="#prod-FunctionDeclaration">FunctionDeclaration</a></emu-nt>, <emu-nt id="_ref_9100"><a href="#prod-FunctionExpression">FunctionExpression</a></emu-nt>, <emu-nt id="_ref_9101"><a href="#prod-GeneratorDeclaration">GeneratorDeclaration</a></emu-nt>, <emu-nt id="_ref_9102"><a href="#prod-GeneratorExpression">GeneratorExpression</a></emu-nt>, <emu-nt id="_ref_9103"><a href="#prod-AsyncFunctionDeclaration">AsyncFunctionDeclaration</a></emu-nt>, <emu-nt id="_ref_9104"><a href="#prod-AsyncFunctionExpression">AsyncFunctionExpression</a></emu-nt>, <emu-nt id="_ref_9105"><a href="#prod-AsyncGeneratorDeclaration">AsyncGeneratorDeclaration</a></emu-nt>, <emu-nt id="_ref_9106"><a href="#prod-AsyncGeneratorExpression">AsyncGeneratorExpression</a></emu-nt>, <emu-nt id="_ref_9107"><a href="#prod-MethodDefinition">MethodDefinition</a></emu-nt>, <emu-nt id="_ref_9108"><a href="#prod-ArrowFunction">ArrowFunction</a></emu-nt>, <emu-nt id="_ref_9109"><a href="#prod-AsyncArrowFunction">AsyncArrowFunction</a></emu-nt>, <emu-nt id="_ref_9110"><a href="#prod-ClassDeclaration">ClassDeclaration</a></emu-nt>, or <emu-nt id="_ref_9111"><a href="#prod-ClassExpression">ClassExpression</a></emu-nt>.
      
      </li>
      <li>
        <em>Eval code</em> is the source text supplied to the built-in <code>eval</code> function. More precisely, if the parameter to the built-in <code>eval</code> function is a String, it is treated as an ECMAScript <emu-nt id="_ref_9112"><a href="#prod-Script">Script</a></emu-nt>. The eval code for a particular invocation of <code>eval</code> is the global code portion of that <emu-nt id="_ref_9113"><a href="#prod-Script">Script</a></emu-nt>.
      
      </li>
      <li>
        <em>Function code</em> is source text that is parsed to supply the value of the [[ECMAScriptCode]] and [[FormalParameters]] internal slots (see  <emu-xref href="#sec-ecmascript-function-objects" id="_ref_254"><a href="#sec-ecmascript-function-objects">9.2</a></emu-xref>) of an ECMAScript <emu-xref href="#function-object" id="_ref_3099"><a href="#function-object">function object</a></emu-xref>.
 The function code of a particular ECMAScript function does not include 
any source text that is parsed as the function code of a nested <emu-nt id="_ref_9114"><a href="#prod-FunctionDeclaration">FunctionDeclaration</a></emu-nt>, <emu-nt id="_ref_9115"><a href="#prod-FunctionExpression">FunctionExpression</a></emu-nt>, <emu-nt id="_ref_9116"><a href="#prod-GeneratorDeclaration">GeneratorDeclaration</a></emu-nt>, <emu-nt id="_ref_9117"><a href="#prod-GeneratorExpression">GeneratorExpression</a></emu-nt>, <emu-nt id="_ref_9118"><a href="#prod-AsyncFunctionDeclaration">AsyncFunctionDeclaration</a></emu-nt>, <emu-nt id="_ref_9119"><a href="#prod-AsyncFunctionExpression">AsyncFunctionExpression</a></emu-nt>, <emu-nt id="_ref_9120"><a href="#prod-AsyncGeneratorDeclaration">AsyncGeneratorDeclaration</a></emu-nt>, <emu-nt id="_ref_9121"><a href="#prod-AsyncGeneratorExpression">AsyncGeneratorExpression</a></emu-nt>, <emu-nt id="_ref_9122"><a href="#prod-MethodDefinition">MethodDefinition</a></emu-nt>, <emu-nt id="_ref_9123"><a href="#prod-ArrowFunction">ArrowFunction</a></emu-nt>, <emu-nt id="_ref_9124"><a href="#prod-AsyncArrowFunction">AsyncArrowFunction</a></emu-nt>, <emu-nt id="_ref_9125"><a href="#prod-ClassDeclaration">ClassDeclaration</a></emu-nt>, or <emu-nt id="_ref_9126"><a href="#prod-ClassExpression">ClassExpression</a></emu-nt>.
      
      </li>
      <li>
        <em>Module code</em> is source text that is code that is provided as a <emu-nt id="_ref_9127"><a href="#prod-ModuleBody">ModuleBody</a></emu-nt>.
 It is the code that is directly evaluated when a module is initialized.
 The module code of a particular module does not include any source text
 that is parsed as part of a nested <emu-nt id="_ref_9128"><a href="#prod-FunctionDeclaration">FunctionDeclaration</a></emu-nt>, <emu-nt id="_ref_9129"><a href="#prod-FunctionExpression">FunctionExpression</a></emu-nt>, <emu-nt id="_ref_9130"><a href="#prod-GeneratorDeclaration">GeneratorDeclaration</a></emu-nt>, <emu-nt id="_ref_9131"><a href="#prod-GeneratorExpression">GeneratorExpression</a></emu-nt>, <emu-nt id="_ref_9132"><a href="#prod-AsyncFunctionDeclaration">AsyncFunctionDeclaration</a></emu-nt>, <emu-nt id="_ref_9133"><a href="#prod-AsyncFunctionExpression">AsyncFunctionExpression</a></emu-nt>, <emu-nt id="_ref_9134"><a href="#prod-AsyncGeneratorDeclaration">AsyncGeneratorDeclaration</a></emu-nt>, <emu-nt id="_ref_9135"><a href="#prod-AsyncGeneratorExpression">AsyncGeneratorExpression</a></emu-nt>, <emu-nt id="_ref_9136"><a href="#prod-MethodDefinition">MethodDefinition</a></emu-nt>, <emu-nt id="_ref_9137"><a href="#prod-ArrowFunction">ArrowFunction</a></emu-nt>, <emu-nt id="_ref_9138"><a href="#prod-AsyncArrowFunction">AsyncArrowFunction</a></emu-nt>, <emu-nt id="_ref_9139"><a href="#prod-ClassDeclaration">ClassDeclaration</a></emu-nt>, or <emu-nt id="_ref_9140"><a href="#prod-ClassExpression">ClassExpression</a></emu-nt>.
      
      </li>
    </ul>
    <emu-note><span class="note">Note</span><div class="note-contents">
      <p>Function code is generally provided as the bodies of Function Definitions (<emu-xref href="#sec-function-definitions" id="_ref_255"><a href="#sec-function-definitions">14.1</a></emu-xref>), Arrow Function Definitions (<emu-xref href="#sec-arrow-function-definitions" id="_ref_256"><a href="#sec-arrow-function-definitions">14.2</a></emu-xref>), Method Definitions (<emu-xref href="#sec-method-definitions" id="_ref_257"><a href="#sec-method-definitions">14.3</a></emu-xref>), Generator Function Definitions (<emu-xref href="#sec-generator-function-definitions" id="_ref_258"><a href="#sec-generator-function-definitions">14.4</a></emu-xref>), Async Function Definitions (<emu-xref href="#sec-async-function-definitions" id="_ref_259"><a href="#sec-async-function-definitions">14.7</a></emu-xref>), Async Generator Function Definitions (<emu-xref href="#sec-async-generator-function-definitions" id="_ref_260"><a href="#sec-async-generator-function-definitions">14.5</a></emu-xref>), and Async Arrow Functions (<emu-xref href="#sec-async-arrow-function-definitions" id="_ref_261"><a href="#sec-async-arrow-function-definitions">14.8</a></emu-xref>). Function code is also derived from the arguments to the <code>Function</code> <emu-xref href="#constructor" id="_ref_3100"><a href="#constructor">constructor</a></emu-xref> (<emu-xref href="#sec-function-p1-p2-pn-body" id="_ref_262"><a href="#sec-function-p1-p2-pn-body">19.2.1.1</a></emu-xref>), the <code>GeneratorFunction</code> <emu-xref href="#constructor" id="_ref_3101"><a href="#constructor">constructor</a></emu-xref> (<emu-xref href="#sec-generatorfunction" id="_ref_263"><a href="#sec-generatorfunction">25.2.1.1</a></emu-xref>), and the <code>AsyncFunction</code> <emu-xref href="#constructor" id="_ref_3102"><a href="#constructor">constructor</a></emu-xref> (<emu-xref href="#sec-async-function-constructor-arguments" id="_ref_264"><a href="#sec-async-function-constructor-arguments">25.7.1.1</a></emu-xref>).</p>
    </div></emu-note>

    <emu-clause id="sec-strict-mode-code">
      <h1><span class="secnum">10.2.1</span>Strict Mode Code</h1>
      <p>An ECMAScript <emu-nt id="_ref_9141"><a href="#prod-Script">Script</a></emu-nt> syntactic unit may be processed using either unrestricted or strict mode syntax and semantics. Code is interpreted as  <dfn>strict mode code</dfn> in the following situations:</p>
      <ul>
        <li>
          Global code is strict mode code if it begins with a <emu-xref href="#directive-prologue" id="_ref_3103"><a href="#directive-prologue">Directive Prologue</a></emu-xref> that contains a <emu-xref href="#use-strict-directive" id="_ref_3104"><a href="#use-strict-directive">Use Strict Directive</a></emu-xref>.
        
        </li>
        <li>
          Module code is always strict mode code.
        
        </li>
        <li>
          All parts of a <emu-nt id="_ref_9142"><a href="#prod-ClassDeclaration">ClassDeclaration</a></emu-nt> or a <emu-nt id="_ref_9143"><a href="#prod-ClassExpression">ClassExpression</a></emu-nt> are strict mode code.
        
        </li>
        <li>
          Eval code is strict mode code if it begins with a <emu-xref href="#directive-prologue" id="_ref_3105"><a href="#directive-prologue">Directive Prologue</a></emu-xref> that contains a <emu-xref href="#use-strict-directive" id="_ref_3106"><a href="#use-strict-directive">Use Strict Directive</a></emu-xref> or if the call to <code>eval</code> is a <emu-xref href="#sec-function-calls-runtime-semantics-evaluation" id="_ref_3107"><a href="#sec-function-calls-runtime-semantics-evaluation">direct eval</a></emu-xref> that is contained in strict mode code.
        
        </li>
        <li>
          Function code is strict mode code if the associated <emu-nt id="_ref_9144"><a href="#prod-FunctionDeclaration">FunctionDeclaration</a></emu-nt>, <emu-nt id="_ref_9145"><a href="#prod-FunctionExpression">FunctionExpression</a></emu-nt>, <emu-nt id="_ref_9146"><a href="#prod-GeneratorDeclaration">GeneratorDeclaration</a></emu-nt>, <emu-nt id="_ref_9147"><a href="#prod-GeneratorExpression">GeneratorExpression</a></emu-nt>, <emu-nt id="_ref_9148"><a href="#prod-AsyncFunctionDeclaration">AsyncFunctionDeclaration</a></emu-nt>, <emu-nt id="_ref_9149"><a href="#prod-AsyncFunctionExpression">AsyncFunctionExpression</a></emu-nt>, <emu-nt id="_ref_9150"><a href="#prod-AsyncGeneratorDeclaration">AsyncGeneratorDeclaration</a></emu-nt>, <emu-nt id="_ref_9151"><a href="#prod-AsyncGeneratorExpression">AsyncGeneratorExpression</a></emu-nt>, <emu-nt id="_ref_9152"><a href="#prod-MethodDefinition">MethodDefinition</a></emu-nt>, <emu-nt id="_ref_9153"><a href="#prod-ArrowFunction">ArrowFunction</a></emu-nt>, or <emu-nt id="_ref_9154"><a href="#prod-AsyncArrowFunction">AsyncArrowFunction</a></emu-nt>
 is contained in strict mode code or if the code that produces the value
 of the function's [[ECMAScriptCode]] internal slot begins with a <emu-xref href="#directive-prologue" id="_ref_3108"><a href="#directive-prologue">Directive Prologue</a></emu-xref> that contains a <emu-xref href="#use-strict-directive" id="_ref_3109"><a href="#use-strict-directive">Use Strict Directive</a></emu-xref>.
        
        </li>
        <li>
          Function code that is supplied as the arguments to the built-in <code>Function</code>, <code>Generator</code>, <code>AsyncFunction</code>, and <code>AsyncGenerator</code> constructors is strict mode code if the last argument is a String that when processed is a <emu-nt id="_ref_9155"><a href="#prod-FunctionBody">FunctionBody</a></emu-nt> that begins with a <emu-xref href="#directive-prologue" id="_ref_3110"><a href="#directive-prologue">Directive Prologue</a></emu-xref> that contains a <emu-xref href="#use-strict-directive" id="_ref_3111"><a href="#use-strict-directive">Use Strict Directive</a></emu-xref>.
        
        </li>
      </ul>
      <p>ECMAScript code that is not strict mode code is called  <dfn id="non-strict-code">non-strict code</dfn>.</p>
    </emu-clause>

    <emu-clause id="sec-non-ecmascript-functions">
      <h1><span class="secnum">10.2.2</span>Non-ECMAScript Functions</h1>
      <p>An ECMAScript implementation may support the evaluation of 
function exotic objects whose evaluative behaviour is expressed in some 
implementation-defined form of executable code other than via ECMAScript
 code. Whether a <emu-xref href="#function-object" id="_ref_3112"><a href="#function-object">function object</a></emu-xref>
 is an ECMAScript code function or a non-ECMAScript function is not 
semantically observable from the perspective of an ECMAScript code 
function that calls or is called by such a non-ECMAScript function.</p>
    </emu-clause>
  </emu-clause>
</emu-clause>

    <nav class="bottom-nav">
      <ul>
        <li><a href="ordinary-and-exotic-objects-behaviours.html">Previous</a></li>
        <li><a href="toc.html">TOC</a></li>
        <li><a href="ecmascript-language-lexical-grammar.html">Next</a></li>
      </ul>
    </nav>
    <script src="../js/script.js"></script>
  </body>
  </html>