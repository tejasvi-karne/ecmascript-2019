<!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="../css/ecmarkup.css" rel="stylesheet">
    <title>ecmascript-standard-built-in-objects</title>
  </head>
  <body>
    <emu-clause id="sec-ecmascript-standard-built-in-objects">
  <h1><span class="secnum">17</span>ECMAScript Standard Built-in Objects</h1>
  <p>There are certain built-in objects available whenever an ECMAScript <emu-nt id="_ref_14289"><a href="#prod-Script">Script</a></emu-nt> or <emu-nt id="_ref_14290"><a href="#prod-Module">Module</a></emu-nt> begins execution. One, the <emu-xref href="#sec-global-object" id="_ref_4767"><a href="#sec-global-object">global object</a></emu-xref>, is part of the lexical environment of the executing program. Others are accessible as initial properties of the <emu-xref href="#sec-global-object" id="_ref_4768"><a href="#sec-global-object">global object</a></emu-xref> or indirectly as properties of accessible built-in objects.</p>
  <p>Unless specified otherwise, a built-in object that is callable as a function is a built-in <emu-xref href="#function-object" id="_ref_4769"><a href="#function-object">function object</a></emu-xref> with the characteristics described in  <emu-xref href="#sec-built-in-function-objects" id="_ref_398"><a href="#sec-built-in-function-objects">9.3</a></emu-xref>. Unless specified otherwise, the [[Extensible]] internal slot of a built-in object initially has the value <emu-val>true</emu-val>. Every built-in <emu-xref href="#function-object" id="_ref_4770"><a href="#function-object">function object</a></emu-xref> has a [[Realm]] internal slot whose value is the <emu-xref href="#realm-record" id="_ref_4771"><a href="#realm-record">Realm Record</a></emu-xref> of the <emu-xref href="#realm" id="_ref_4772"><a href="#realm">realm</a></emu-xref> for which the object was initially created.</p>
  <p>Many built-in objects are functions: they can be invoked with 
arguments. Some of them furthermore are constructors: they are functions
 intended for use with the <code>new</code> operator. For each built-in 
function, this specification describes the arguments required by that 
function and the properties of that <emu-xref href="#function-object" id="_ref_4773"><a href="#function-object">function object</a></emu-xref>. For each built-in <emu-xref href="#constructor" id="_ref_4774"><a href="#constructor">constructor</a></emu-xref>, this specification furthermore describes properties of the prototype object of that <emu-xref href="#constructor" id="_ref_4775"><a href="#constructor">constructor</a></emu-xref> and properties of specific object instances returned by a <code>new</code> expression that invokes that <emu-xref href="#constructor" id="_ref_4776"><a href="#constructor">constructor</a></emu-xref>.</p>
  <p>Unless otherwise specified in the description of a particular function, if a built-in function or <emu-xref href="#constructor" id="_ref_4777"><a href="#constructor">constructor</a></emu-xref> is given fewer arguments than the function is specified to require, the function or <emu-xref href="#constructor" id="_ref_4778"><a href="#constructor">constructor</a></emu-xref> shall behave exactly as if it had been given sufficient additional arguments, each such argument being the <emu-val>undefined</emu-val>
 value. Such missing arguments are considered to be “not present” and 
may be identified in that manner by specification algorithms. In the 
description of a particular function, the terms “<code>this</code> value” and “NewTarget” have the meanings given in  <emu-xref href="#sec-built-in-function-objects" id="_ref_399"><a href="#sec-built-in-function-objects">9.3</a></emu-xref>.</p>
  <p>Unless otherwise specified in the description of a particular function, if a built-in function or <emu-xref href="#constructor" id="_ref_4779"><a href="#constructor">constructor</a></emu-xref>
 described is given more arguments than the function is specified to 
allow, the extra arguments are evaluated by the call and then ignored by
 the function. However, an implementation may define implementation 
specific behaviour relating to such arguments as long as the behaviour 
is not the throwing of a <emu-val>TypeError</emu-val> exception that is predicated simply on the presence of an extra argument.</p>
  <emu-note><span class="note">Note 1</span><div class="note-contents">
    <p>Implementations that add additional capabilities to the set of 
built-in functions are encouraged to do so by adding new functions 
rather than adding new parameters to existing functions.</p>
  </div></emu-note>
  <p>Unless otherwise specified every built-in function and every built-in <emu-xref href="#constructor" id="_ref_4780"><a href="#constructor">constructor</a></emu-xref> has the Function prototype object, which is the initial value of the expression <code>Function.prototype</code> (<emu-xref href="#sec-properties-of-the-function-prototype-object" id="_ref_400"><a href="#sec-properties-of-the-function-prototype-object">19.2.3</a></emu-xref>), as the value of its [[Prototype]] internal slot.</p>
  <p>Unless otherwise specified every built-in prototype object has the 
Object prototype object, which is the initial value of the expression <code>Object.prototype</code> (<emu-xref href="#sec-properties-of-the-object-prototype-object" id="_ref_401"><a href="#sec-properties-of-the-object-prototype-object">19.1.3</a></emu-xref>), as the value of its [[Prototype]] internal slot, except the Object prototype object itself.</p>
  <p>Built-in function objects that are not identified as constructors 
do not implement the [[Construct]] internal method unless otherwise 
specified in the description of a particular function.</p>
  <p>Each built-in function defined in this specification is created by calling the <emu-xref aoid="CreateBuiltinFunction" id="_ref_4781"><a href="#sec-createbuiltinfunction">CreateBuiltinFunction</a></emu-xref> abstract operation (<emu-xref href="#sec-createbuiltinfunction" id="_ref_402"><a href="#sec-createbuiltinfunction">9.3.3</a></emu-xref>).</p>
  <p>Every built-in <emu-xref href="#function-object" id="_ref_4782"><a href="#function-object">function object</a></emu-xref>, including constructors, has a <code>"length"</code>
 property whose value is an integer. Unless otherwise specified, this 
value is equal to the largest number of named arguments shown in the 
subclause headings for the function description. Optional parameters 
(which are indicated with brackets: <code>[</code> <code>]</code>) or rest parameters (which are shown using the form «...name») are not included in the default argument count.</p>
  <emu-note><span class="note">Note 2</span><div class="note-contents">
    <p>For example, the <emu-xref href="#function-object" id="_ref_4783"><a href="#function-object">function object</a></emu-xref> that is the initial value of the <code>map</code>
 property of the Array prototype object is described under the subclause
 heading «Array.prototype.map (callbackFn [ , thisArg])» which shows the
 two named arguments callbackFn and thisArg, the latter being optional; 
therefore the value of the <code>"length"</code> property of that <emu-xref href="#function-object" id="_ref_4784"><a href="#function-object">function object</a></emu-xref> is 1.</p>
  </div></emu-note>
  <p>Unless otherwise specified, the <code>"length"</code> property of a built-in <emu-xref href="#function-object" id="_ref_4785"><a href="#function-object">function object</a></emu-xref> has the attributes { [[Writable]]: <emu-val>false</emu-val>, [[Enumerable]]: <emu-val>false</emu-val>, [[Configurable]]: <emu-val>true</emu-val> }.</p>
  <p>Every built-in <emu-xref href="#function-object" id="_ref_4786"><a href="#function-object">function object</a></emu-xref>, including constructors, that is not identified as an anonymous function has a <code>name</code>
 property whose value is a String. Unless otherwise specified, this 
value is the name that is given to the function in this specification. 
For functions that are specified as properties of objects, the name 
value is the <emu-xref href="#property-name" id="_ref_4787"><a href="#property-name">property name</a></emu-xref> string used to access the function. Functions that are specified as get or set accessor functions of built-in properties have <code>"get "</code> or <code>"set "</code> prepended to the <emu-xref href="#property-name" id="_ref_4788"><a href="#property-name">property name</a></emu-xref> string. The value of the <code>name</code> property is explicitly specified for each built-in functions whose property key is a Symbol value.</p>
  <p>Unless otherwise specified, the <code>name</code> property of a built-in <emu-xref href="#function-object" id="_ref_4789"><a href="#function-object">function object</a></emu-xref>, if it exists, has the attributes { [[Writable]]: <emu-val>false</emu-val>, [[Enumerable]]: <emu-val>false</emu-val>, [[Configurable]]: <emu-val>true</emu-val> }.</p>
  <p>Every other <emu-xref href="#sec-object-type" id="_ref_4790"><a href="#sec-object-type">data property</a></emu-xref> described in clauses 18 through 26 and in Annex  <emu-xref href="#sec-additional-built-in-properties" id="_ref_403"><a href="#sec-additional-built-in-properties">B.2</a></emu-xref> has the attributes { [[Writable]]: <emu-val>true</emu-val>, [[Enumerable]]: <emu-val>false</emu-val>, [[Configurable]]: <emu-val>true</emu-val> } unless otherwise specified.</p>
  <p>Every <emu-xref href="#sec-object-type" id="_ref_4791"><a href="#sec-object-type">accessor property</a></emu-xref> described in clauses 18 through 26 and in Annex  <emu-xref href="#sec-additional-built-in-properties" id="_ref_404"><a href="#sec-additional-built-in-properties">B.2</a></emu-xref> has the attributes { [[Enumerable]]: <emu-val>false</emu-val>, [[Configurable]]: <emu-val>true</emu-val> } unless otherwise specified. If only a get accessor function is described, the set accessor function is the default value, <emu-val>undefined</emu-val>. If only a set accessor is described the get accessor is the default value, <emu-val>undefined</emu-val>.</p>
</emu-clause>

    <nav class="bottom-nav">
      <ul>
        <li><a href="error-handling-and-language-extensions.html">Previous</a></li>
        <li><a href="toc.html">TOC</a></li>
        <li><a href="global-object.html">Next</a></li>
      </ul>
    </nav>
    <script src="../js/script.js"></script>
  </body>
  </html>