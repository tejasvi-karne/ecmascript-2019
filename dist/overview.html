<!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="../css/ecmarkup.css" rel="stylesheet">
    <title>overview</title>
  </head>
  <body>
    <emu-clause id="sec-overview">
  <h1><span class="secnum">4</span>Overview</h1>
  <p>This section contains a non-normative overview of the ECMAScript language.</p>
  <p>ECMAScript is an object-oriented programming language for 
performing computations and manipulating computational objects within a 
host environment. ECMAScript as defined here is not intended to be 
computationally self-sufficient; indeed, there are no provisions in this
 specification for input of external data or output of computed results.
 Instead, it is expected that the computational environment of an 
ECMAScript program will provide not only the objects and other 
facilities described in this specification but also certain 
environment-specific objects, whose description and behaviour are beyond
 the scope of this specification except to indicate that they may 
provide certain properties that can be accessed and certain functions 
that can be called from an ECMAScript program.</p>
  <p>ECMAScript was originally designed to be used as a scripting 
language, but has become widely used as a general-purpose programming 
language. A  <em>scripting language</em> is a programming language that 
is used to manipulate, customize, and automate the facilities of an 
existing system. In such systems, useful functionality is already 
available through a user interface, and the scripting language is a 
mechanism for exposing that functionality to program control. In this 
way, the existing system is said to provide a host environment of 
objects and facilities, which completes the capabilities of the 
scripting language. A scripting language is intended for use by both 
professional and non-professional programmers.</p>
  <p>ECMAScript was originally designed to be a  <em>Web scripting language</em>,
 providing a mechanism to enliven Web pages in browsers and to perform 
server computation as part of a Web-based client-server architecture. 
ECMAScript is now used to provide core scripting capabilities for a 
variety of host environments. Therefore the core language is specified 
in this document apart from any particular host environment.</p>
  <p>ECMAScript usage has moved beyond simple scripting and it is now 
used for the full spectrum of programming tasks in many different 
environments and scales. As the usage of ECMAScript has expanded, so has
 the features and facilities it provides. ECMAScript is now a fully 
featured general-purpose programming language.</p>
  <p>Some of the facilities of ECMAScript are similar to those used in 
other programming languages; in particular C, Java™, Self, and Scheme as
 described in:</p>
  <p>ISO/IEC 9899:1996,  <i>Programming Languages – C</i>.</p>
  <p>Gosling, James, Bill Joy and Guy Steele.  <i>The Java<sup>™</sup> Language Specification</i>. Addison Wesley Publishing Co., 1996.</p>
  <p>Ungar, David, and Smith, Randall B. Self: The Power of Simplicity.  <i>OOPSLA '87 Conference Proceedings</i>, pp. 227-241, Orlando, FL, October 1987.</p>
  <p><i>IEEE Standard for the Scheme Programming Language</i>. IEEE Std 1178-1990.</p>

  <emu-clause id="sec-web-scripting">
    <h1><span class="secnum">4.1</span>Web Scripting</h1>
    <p>A web browser provides an ECMAScript host environment for 
client-side computation including, for instance, objects that represent 
windows, menus, pop-ups, dialog boxes, text areas, anchors, frames, 
history, cookies, and input/output. Further, the host environment 
provides a means to attach scripting code to events such as change of 
focus, page and image loading, unloading, error and abort, selection, 
form submission, and mouse actions. Scripting code appears within the 
HTML and the displayed page is a combination of user interface elements 
and fixed and computed text and images. The scripting code is reactive 
to user interaction, and there is no need for a main program.</p>
    <p>A web server provides a different host environment for 
server-side computation including objects representing requests, 
clients, and files; and mechanisms to lock and share data. By using 
browser-side and server-side scripting together, it is possible to 
distribute computation between the client and server while providing a 
customized user interface for a Web-based application.</p>
    <p>Each Web browser and server that supports ECMAScript supplies its
 own host environment, completing the ECMAScript execution environment.</p>
  </emu-clause>

  <emu-clause id="sec-ecmascript-overview">
    <h1><span class="secnum">4.2</span>ECMAScript Overview</h1>
    <p>The following is an informal overview of ECMAScript—not all parts
 of the language are described. This overview is not part of the 
standard proper.</p>
    <p>ECMAScript is object-based: basic language and host facilities 
are provided by objects, and an ECMAScript program is a cluster of 
communicating objects. In ECMAScript, an  <em>object</em> is a collection of zero or more  <em>properties</em> each with  <em>attributes</em> that determine how each property can be used—for example, when the Writable attribute for a property is set to <emu-val>false</emu-val>,
 any attempt by executed ECMAScript code to assign a different value to 
the property fails. Properties are containers that hold other objects,  <em>primitive values</em>, or  <em>functions</em>. A primitive value is a member of one of the following built-in types:  <b>Undefined</b>,  <b>Null</b>,  <b>Boolean</b>,  <b>Number</b>,  <b>String</b>, and  <b>Symbol;</b> an object is a member of the built-in type  <b>Object</b>; and a function is a callable object. A function that is associated with an object via a property is called a  <em>method</em>.</p>
    <p>ECMAScript defines a collection of  <em>built-in objects</em> that round out the definition of ECMAScript entities. These built-in objects include the <emu-xref href="#sec-global-object" id="_ref_857"><a href="#sec-global-object">global object</a></emu-xref>; objects that are fundamental to the <emu-xref href="#sec-runtime-semantics" id="_ref_858"><a href="#sec-runtime-semantics">runtime semantics</a></emu-xref> of the language including <code>Object</code>, <code>Function</code>, <code>Boolean</code>, <code>Symbol</code>, and various <code>Error</code> objects; objects that represent and manipulate numeric values including <code>Math</code>, <code>Number</code>, and <code>Date</code>; the text processing objects <code>String</code> and <code>RegExp</code>; objects that are indexed collections of values including <code>Array</code>
 and nine different kinds of Typed Arrays whose elements all have a 
specific numeric data representation; keyed collections including <code>Map</code> and <code>Set</code> objects; objects supporting structured data including the <code>JSON</code> object, <code>ArrayBuffer</code>, <code>SharedArrayBuffer</code>, and <code>DataView</code>; objects supporting control abstractions including generator functions and <code>Promise</code> objects; and reflection objects including <code>Proxy</code> and <code>Reflect</code>.</p>
    <p>ECMAScript also defines a set of built-in  <em>operators</em>. 
ECMAScript operators include various unary operations, multiplicative 
operators, additive operators, bitwise shift operators, relational 
operators, equality operators, binary bitwise operators, binary logical 
operators, assignment operators, and the comma operator.</p>
    <p>Large ECMAScript programs are supported by  <em>modules</em> 
which allow a program to be divided into multiple sequences of 
statements and declarations. Each module explicitly identifies 
declarations it uses that need to be provided by other modules and which
 of its declarations are available for use by other modules.</p>
    <p>ECMAScript syntax intentionally resembles Java syntax. ECMAScript
 syntax is relaxed to enable it to serve as an easy-to-use scripting 
language. For example, a variable is not required to have its type 
declared nor are types associated with properties, and defined functions
 are not required to have their declarations appear textually before 
calls to them.</p>

    <emu-clause id="sec-objects">
      <h1><span class="secnum">4.2.1</span>Objects</h1>
      <p>Even though ECMAScript includes syntax for class definitions, 
ECMAScript objects are not fundamentally class-based such as those in 
C++, Smalltalk, or Java. Instead objects may be created in various ways 
including via a literal notation or via  <em>constructors</em> which 
create objects and then execute code that initializes all or part of 
them by assigning initial values to their properties. Each <emu-xref href="#constructor" id="_ref_859"><a href="#constructor">constructor</a></emu-xref> is a function that has a property named <code>"prototype"</code> that is used to implement  <em>prototype-based inheritance</em> and  <em>shared properties</em>. Objects are created by using constructors in  <b>new</b> expressions; for example, <code>new Date(2009, 11)</code> creates a new Date object. Invoking a <emu-xref href="#constructor" id="_ref_860"><a href="#constructor">constructor</a></emu-xref> without using  <b>new</b> has consequences that depend on the <emu-xref href="#constructor" id="_ref_861"><a href="#constructor">constructor</a></emu-xref>. For example, <code>Date()</code> produces a string representation of the current date and time rather than an object.</p>
      <p>Every object created by a <emu-xref href="#constructor" id="_ref_862"><a href="#constructor">constructor</a></emu-xref> has an implicit reference (called the object's  <em>prototype</em>) to the value of its <emu-xref href="#constructor" id="_ref_863"><a href="#constructor">constructor</a></emu-xref>'s <code>"prototype"</code> property. Furthermore, a prototype may have a non-null implicit reference to its prototype, and so on; this is called the  <em>prototype chain</em>.
 When a reference is made to a property in an object, that reference is 
to the property of that name in the first object in the prototype chain 
that contains a property of that name. In other words, first the object 
mentioned directly is examined for such a property; if that object 
contains the named property, that is the property to which the reference
 refers; if that object does not contain the named property, the 
prototype for that object is examined next; and so on.</p>
      <emu-figure id="figure-1" caption="Object/Prototype Relationships"><figure><figcaption>Figure 1: Object/Prototype Relationships</figcaption>
        <object data="ECMAScript%C2%AE%202019%20Language%C2%A0Specification_files/figure-1.svg" type="image/svg+xml" width="719" height="354"> <img alt="An image of lots of boxes and arrows." src="../assets/figure-1.png" width="719" height="354"> </object>
      </figure></emu-figure>
      <p>In a class-based object-oriented language, in general, state is
 carried by instances, methods are carried by classes, and inheritance 
is only of structure and behaviour. In ECMAScript, the state and methods
 are carried by objects, while structure, behaviour, and state are all 
inherited.</p>
      <p>All objects that do not directly contain a particular property 
that their prototype contains share that property and its value. Figure 1
 illustrates this:</p>
      <p><b>CF</b> is a <emu-xref href="#constructor" id="_ref_864"><a href="#constructor">constructor</a></emu-xref> (and also an object). Five objects have been created by using <code>new</code> expressions:  <b>cf<sub>1</sub></b>,  <b>cf<sub>2</sub></b>,  <b>cf<sub>3</sub></b>,  <b>cf<sub>4</sub></b>, and  <b>cf<sub>5</sub></b>. Each of these objects contains properties named <code>q1</code> and <code>q2</code>. The dashed lines represent the implicit prototype relationship; so, for example,  <b>cf<sub>3</sub></b>'s prototype is  <b>CF<sub>p</sub></b>. The <emu-xref href="#constructor" id="_ref_865"><a href="#constructor">constructor</a></emu-xref>,  <b>CF</b>, has two properties itself, named <code>P1</code> and <code>P2</code>, which are not visible to  <b>CF<sub>p</sub></b>,  <b>cf<sub>1</sub></b>,  <b>cf<sub>2</sub></b>,  <b>cf<sub>3</sub></b>,  <b>cf<sub>4</sub></b>, or  <b>cf<sub>5</sub></b>. The property named <code>CFP1</code> in  <b>CF<sub>p</sub></b> is shared by  <b>cf<sub>1</sub></b>,  <b>cf<sub>2</sub></b>,  <b>cf<sub>3</sub></b>,  <b>cf<sub>4</sub></b>, and  <b>cf<sub>5</sub></b> (but not by  <b>CF</b>), as are any properties found in  <b>CF<sub>p</sub></b>'s implicit prototype chain that are not named <code>q1</code>, <code>q2</code>, or <code>CFP1</code>. Notice that there is no implicit prototype link between  <b>CF</b> and  <b>CF<sub>p</sub></b>.</p>
      <p>Unlike most class-based object languages, properties can be 
added to objects dynamically by assigning values to them. That is, 
constructors are not required to name or assign values to all or any of 
the constructed object's properties. In the above diagram, one could add
 a new shared property for  <b>cf<sub>1</sub></b>,  <b>cf<sub>2</sub></b>,  <b>cf<sub>3</sub></b>,  <b>cf<sub>4</sub></b>, and  <b>cf<sub>5</sub></b> by assigning a new value to the property in  <b>CF<sub>p</sub></b>.</p>
      <p>Although ECMAScript objects are not inherently class-based, it 
is often convenient to define class-like abstractions based upon a 
common pattern of <emu-xref href="#constructor" id="_ref_866"><a href="#constructor">constructor</a></emu-xref>
 functions, prototype objects, and methods. The ECMAScript built-in 
objects themselves follow such a class-like pattern. Beginning with 
ECMAScript 2015, the ECMAScript language includes syntactic class 
definitions that permit programmers to concisely define objects that 
conform to the same class-like abstraction pattern used by the built-in 
objects.</p>
    </emu-clause>

    <emu-clause id="sec-strict-variant-of-ecmascript">
      <h1><span class="secnum">4.2.2</span>The Strict Variant of ECMAScript</h1>
      <p>The ECMAScript Language recognizes the possibility that some 
users of the language may wish to restrict their usage of some features 
available in the language. They might do so in the interests of 
security, to avoid what they consider to be error-prone features, to get
 enhanced error checking, or for other reasons of their choosing. In 
support of this possibility, ECMAScript defines a strict variant of the 
language. The strict variant of the language excludes some specific 
syntactic and semantic features of the regular ECMAScript language and 
modifies the detailed semantics of some features. The strict variant 
also specifies additional error conditions that must be reported by 
throwing error exceptions in situations that are not specified as errors
 by the non-strict form of the language.</p>
      <p>The strict variant of ECMAScript is commonly referred to as the  <em>strict mode</em>
 of the language. Strict mode selection and use of the strict mode 
syntax and semantics of ECMAScript is explicitly made at the level of 
individual ECMAScript source text units. Because strict mode is selected
 at the level of a syntactic source text unit, strict mode only imposes 
restrictions that have local effect within such a source text unit. 
Strict mode does not restrict or modify any aspect of the ECMAScript 
semantics that must operate consistently across multiple source text 
units. A complete ECMAScript program may be composed of both strict mode
 and non-strict mode ECMAScript source text units. In this case, strict 
mode only applies when actually executing code that is defined within a 
strict mode source text unit.</p>
      <p>In order to conform to this specification, an ECMAScript 
implementation must implement both the full unrestricted ECMAScript 
language and the strict variant of the ECMAScript language as defined by
 this specification. In addition, an implementation must support the 
combination of unrestricted and strict mode source text units into a 
single composite program.</p>
    </emu-clause>
  </emu-clause>

  <emu-clause id="sec-terms-and-definitions">
    <h1><span class="secnum">4.3</span>Terms and Definitions</h1>
    <p>For the purposes of this document, the following terms and definitions apply.</p>

    <emu-clause id="sec-type">
      <h1><span class="secnum">4.3.1</span>type</h1>
      <p>set of data values as defined in clause  <emu-xref href="#sec-ecmascript-data-types-and-values" id="_ref_3"><a href="#sec-ecmascript-data-types-and-values">6</a></emu-xref> of this specification</p>
    </emu-clause>

    <emu-clause id="sec-primitive-value">
      <h1><span class="secnum">4.3.2</span>primitive value</h1>
      <p>member of one of the types Undefined, Null, Boolean, Number, Symbol, or String as defined in clause  <emu-xref href="#sec-ecmascript-data-types-and-values" id="_ref_4"><a href="#sec-ecmascript-data-types-and-values">6</a></emu-xref></p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>A primitive value is a datum that is represented directly at the lowest level of the language implementation.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-object">
      <h1><span class="secnum">4.3.3</span>object</h1>
      <p>member of the type Object</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>An object is a collection of properties and has a single prototype object. The prototype may be the null value.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-constructor">
      <h1><span class="secnum">4.3.4</span>constructor</h1>
      <p><emu-xref href="#function-object" id="_ref_867"><a href="#function-object">function object</a></emu-xref> that creates and initializes objects</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>The value of a <emu-xref href="#constructor" id="_ref_868"><a href="#constructor">constructor</a></emu-xref>'s <code>prototype</code> property is a prototype object that is used to implement inheritance and shared properties.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-prototype">
      <h1><span class="secnum">4.3.5</span>prototype</h1>
      <p>object that provides shared properties for other objects</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>When a <emu-xref href="#constructor" id="_ref_869"><a href="#constructor">constructor</a></emu-xref> creates an object, that object implicitly references the <emu-xref href="#constructor" id="_ref_870"><a href="#constructor">constructor</a></emu-xref>'s <code>prototype</code> property for the purpose of resolving property references. The <emu-xref href="#constructor" id="_ref_871"><a href="#constructor">constructor</a></emu-xref>'s <code>prototype</code> property can be referenced by the program expression  <code><var>constructor</var>.prototype</code>,
 and properties added to an object's prototype are shared, through 
inheritance, by all objects sharing the prototype. Alternatively, a new 
object may be created with an explicitly specified prototype by using 
the <code>Object.create</code> built-in function.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-ordinary-object">
      <h1><span class="secnum">4.3.6</span>ordinary object</h1>
      <p>object that has the default behaviour for the essential internal methods that must be supported by all objects</p>
    </emu-clause>

    <emu-clause id="sec-exotic-object">
      <h1><span class="secnum">4.3.7</span>exotic object</h1>
      <p>object that does not have the default behaviour for one or more of the essential internal methods</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>Any object that is not an ordinary object is an <emu-xref href="#exotic-object" id="_ref_872"><a href="#exotic-object">exotic object</a></emu-xref>.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-standard-object">
      <h1><span class="secnum">4.3.8</span>standard object</h1>
      <p>object whose semantics are defined by this specification</p>
    </emu-clause>

    <emu-clause id="sec-built-in-object">
      <h1><span class="secnum">4.3.9</span>built-in object</h1>
      <p>object specified and supplied by an ECMAScript implementation</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>Standard built-in objects are defined in this specification. 
An ECMAScript implementation may specify and supply additional kinds of 
built-in objects. A  <em>built-in <emu-xref href="#constructor" id="_ref_873"><a href="#constructor">constructor</a></emu-xref></em> is a built-in object that is also a <emu-xref href="#constructor" id="_ref_874"><a href="#constructor">constructor</a></emu-xref>.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-undefined-value">
      <h1><span class="secnum">4.3.10</span>undefined value</h1>
      <p>primitive value used when a variable has not been assigned a value</p>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-undefined-type">
      <h1><span class="secnum">4.3.11</span>Undefined type</h1>
      <p>type whose sole value is the <emu-val>undefined</emu-val> value</p>
    </emu-clause>

    <emu-clause id="sec-null-value">
      <h1><span class="secnum">4.3.12</span>null value</h1>
      <p>primitive value that represents the intentional absence of any object value</p>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-null-type">
      <h1><span class="secnum">4.3.13</span>Null type</h1>
      <p>type whose sole value is the <emu-val>null</emu-val> value</p>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-boolean-value">
      <h1><span class="secnum">4.3.14</span>Boolean value</h1>
      <p>member of the Boolean type</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>There are only two Boolean values, <emu-val>true</emu-val> and <emu-val>false</emu-val>.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-boolean-type">
      <h1><span class="secnum">4.3.15</span>Boolean type</h1>
      <p>type consisting of the primitive values <emu-val>true</emu-val> and <emu-val>false</emu-val></p>
    </emu-clause>

    <emu-clause id="sec-boolean-object">
      <h1><span class="secnum">4.3.16</span>Boolean object</h1>
      <p>member of the Object type that is an instance of the standard built-in <code>Boolean</code> <emu-xref href="#constructor" id="_ref_875"><a href="#constructor">constructor</a></emu-xref></p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>A Boolean object is created by using the <code>Boolean</code> <emu-xref href="#constructor" id="_ref_876"><a href="#constructor">constructor</a></emu-xref> in a <code>new</code>
 expression, supplying a Boolean value as an argument. The resulting 
object has an internal slot whose value is the Boolean value. A Boolean 
object can be coerced to a Boolean value.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-string-value">
      <h1><span class="secnum">4.3.17</span>String value</h1>
      <p>primitive value that is a finite ordered sequence of zero or more 16-bit unsigned integer values</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>A String value is a member of the String type. Each integer 
value in the sequence usually represents a single 16-bit unit of UTF-16 
text. However, ECMAScript does not place any restrictions or 
requirements on the values except that they must be 16-bit unsigned 
integers.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-string-type">
      <h1><span class="secnum">4.3.18</span>String type</h1>
      <p>set of all possible String values</p>
    </emu-clause>

    <emu-clause id="sec-string-object">
      <h1><span class="secnum">4.3.19</span>String object</h1>
      <p>member of the Object type that is an instance of the standard built-in <code>String</code> <emu-xref href="#constructor" id="_ref_877"><a href="#constructor">constructor</a></emu-xref></p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>A String object is created by using the <code>String</code> <emu-xref href="#constructor" id="_ref_878"><a href="#constructor">constructor</a></emu-xref> in a <code>new</code>
 expression, supplying a String value as an argument. The resulting 
object has an internal slot whose value is the String value. A String 
object can be coerced to a String value by calling the <code>String</code> <emu-xref href="#constructor" id="_ref_879"><a href="#constructor">constructor</a></emu-xref> as a function (<emu-xref href="#sec-string-constructor-string-value" id="_ref_5"><a href="#sec-string-constructor-string-value">21.1.1.1</a></emu-xref>).</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-number-value">
      <h1><span class="secnum">4.3.20</span>Number value</h1>
      <p>primitive value corresponding to a double-precision 64-bit binary format IEEE 754-2008 value</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>A Number value is a member of the Number type and is a direct representation of a number.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-number-type">
      <h1><span class="secnum">4.3.21</span>Number type</h1>
      <p>set of all possible Number values including the special “Not-a-Number” (NaN) value, positive infinity, and negative infinity</p>
    </emu-clause>

    <emu-clause id="sec-number-object">
      <h1><span class="secnum">4.3.22</span>Number object</h1>
      <p>member of the Object type that is an instance of the standard built-in <code>Number</code> <emu-xref href="#constructor" id="_ref_880"><a href="#constructor">constructor</a></emu-xref></p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>A Number object is created by using the <code>Number</code> <emu-xref href="#constructor" id="_ref_881"><a href="#constructor">constructor</a></emu-xref> in a <code>new</code>
 expression, supplying a number value as an argument. The resulting 
object has an internal slot whose value is the number value. A Number 
object can be coerced to a number value by calling the <code>Number</code> <emu-xref href="#constructor" id="_ref_882"><a href="#constructor">constructor</a></emu-xref> as a function (<emu-xref href="#sec-number-constructor-number-value" id="_ref_6"><a href="#sec-number-constructor-number-value">20.1.1.1</a></emu-xref>).</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-infinity">
      <h1><span class="secnum">4.3.23</span>Infinity</h1>
      <p>number value that is the positive infinite number value</p>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-nan">
      <h1><span class="secnum">4.3.24</span>NaN</h1>
      <p>number value that is an IEEE 754-2008 “Not-a-Number” value</p>
    </emu-clause>

    <emu-clause id="sec-symbol-value">
      <h1><span class="secnum">4.3.25</span>Symbol value</h1>
      <p>primitive value that represents a unique, non-String Object property key</p>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-symbol-type">
      <h1><span class="secnum">4.3.26</span>Symbol type</h1>
      <p>set of all possible Symbol values</p>
    </emu-clause>

    <emu-clause id="sec-symbol-object">
      <h1><span class="secnum">4.3.27</span>Symbol object</h1>
      <p>member of the Object type that is an instance of the standard built-in <code>Symbol</code> <emu-xref href="#constructor" id="_ref_883"><a href="#constructor">constructor</a></emu-xref></p>
    </emu-clause>

    <emu-clause id="sec-terms-and-definitions-function">
      <h1><span class="secnum">4.3.28</span>function</h1>
      <p>member of the Object type that may be invoked as a subroutine</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>In addition to its properties, a function contains executable
 code and state that determine how it behaves when invoked. A function's
 code may or may not be written in ECMAScript.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-built-in-function">
      <h1><span class="secnum">4.3.29</span>built-in function</h1>
      <p>built-in object that is a function</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>Examples of built-in functions include <code>parseInt</code> and <code>Math.exp</code>. An implementation may provide implementation-dependent built-in functions that are not described in this specification.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-property">
      <h1><span class="secnum">4.3.30</span>property</h1>
      <p>part of an object that associates a key (either a String value or a Symbol value) and a value</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>Depending upon the form of the property the value may be 
represented either directly as a data value (a primitive value, an 
object, or a <emu-xref href="#function-object" id="_ref_884"><a href="#function-object">function object</a></emu-xref>) or indirectly by a pair of accessor functions.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-method">
      <h1><span class="secnum">4.3.31</span>method</h1>
      <p>function that is the value of a property</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>When a function is called as a method of an object, the object is passed to the function as its <emu-val>this</emu-val> value.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-built-in-method">
      <h1><span class="secnum">4.3.32</span>built-in method</h1>
      <p>method that is a built-in function</p>
      <emu-note><span class="note">Note</span><div class="note-contents">
        <p>Standard built-in methods are defined in this specification, 
and an ECMAScript implementation may specify and provide other 
additional built-in methods.</p>
      </div></emu-note>
    </emu-clause>

    <emu-clause id="sec-attribute">
      <h1><span class="secnum">4.3.33</span>attribute</h1>
      <p>internal value that defines some characteristic of a property</p>
    </emu-clause>

    <emu-clause id="sec-own-property">
      <h1><span class="secnum">4.3.34</span>own property</h1>
      <p>property that is directly contained by its object</p>
    </emu-clause>

    <emu-clause id="sec-inherited-property">
      <h1><span class="secnum">4.3.35</span>inherited property</h1>
      <p>property of an object that is not an own property but is a property (either own or inherited) of the object's prototype</p>
    </emu-clause>
  </emu-clause>

  <emu-clause id="sec-organization-of-this-specification">
    <h1><span class="secnum">4.4</span>Organization of This Specification</h1>
    <p>The remainder of this specification is organized as follows:</p>
    <p>Clause 5 defines the notational conventions used throughout the specification.</p>
    <p>Clauses 6-9 define the execution environment within which ECMAScript programs operate.</p>
    <p>Clauses 10-16 define the actual ECMAScript programming language 
including its syntactic encoding and the execution semantics of all 
language features.</p>
    <p>Clauses 17-26 define the ECMAScript standard library. They 
include the definitions of all of the standard objects that are 
available for use by ECMAScript programs as they execute.</p>
    <p>Clause 27 describes the memory consistency model of accesses on 
SharedArrayBuffer-backed memory and methods of the Atomics object.</p>
  </emu-clause>
</emu-clause>

    <nav class="bottom-nav">
      <ul>
        <li><a href="normative-references.html">Previous</a></li>
        <li><a href="toc.html">TOC</a></li>
        <li><a href="notational-conventions.html">Next</a></li>
      </ul>
    </nav>
    <script src="../js/script.js"></script>
  </body>
  </html>