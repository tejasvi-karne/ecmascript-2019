<emu-clause id="sec-error-handling-and-language-extensions">
  <h1><span class="secnum">16</span>Error Handling and Language Extensions</h1>
  <p>An implementation must report most errors at the time the relevant ECMAScript language construct is evaluated. An  <dfn id="early-error">early error</dfn> is an error that can be detected and reported prior to the evaluation of any construct in the <emu-nt id="_ref_14264"><a href="#prod-Script">Script</a></emu-nt> containing the error. The presence of an <emu-xref href="#early-error" id="_ref_4749"><a href="#early-error">early error</a></emu-xref> prevents the evaluation of the construct. An implementation must report early errors in a <emu-nt id="_ref_14265"><a href="#prod-Script">Script</a></emu-nt> as part of parsing that <emu-nt id="_ref_14266"><a href="#prod-Script">Script</a></emu-nt> in <emu-xref aoid="ParseScript" id="_ref_4750"><a href="#sec-parse-script">ParseScript</a></emu-xref>. Early errors in a <emu-nt id="_ref_14267"><a href="#prod-Module">Module</a></emu-nt> are reported at the point when the <emu-nt id="_ref_14268"><a href="#prod-Module">Module</a></emu-nt> would be evaluated and the <emu-nt id="_ref_14269"><a href="#prod-Module">Module</a></emu-nt> is never initialized. Early errors in  <b>eval</b> code are reported at the time <code>eval</code> is called and prevent evaluation of the  <b>eval</b> code. All errors that are not early errors are runtime errors.</p>
  <p>An implementation must report as an <emu-xref href="#early-error" id="_ref_4751"><a href="#early-error">early error</a></emu-xref> any occurrence of a condition that is listed in a “Static Semantics: Early Errors” subclause of this specification.</p>
  <p>An implementation shall not treat other kinds of errors as early 
errors even if the compiler can prove that a construct cannot execute 
without error under any circumstances. An implementation may issue an 
early warning in such a case, but it should not report the error until 
the relevant construct is actually executed.</p>
  <p>An implementation shall report all errors as specified, except for the following:</p>
  <ul>
    <li>
      Except as restricted in  <emu-xref href="#sec-forbidden-extensions" id="_ref_384"><a href="#sec-forbidden-extensions">16.2</a></emu-xref>, an implementation may extend <emu-nt id="_ref_14270"><a href="#prod-Script">Script</a></emu-nt> syntax, <emu-nt id="_ref_14271"><a href="#prod-Module">Module</a></emu-nt> syntax, and regular expression pattern or flag syntax. To permit this, all operations (such as calling <code>eval</code>, using a regular expression literal, or using the <code>Function</code> or <code>RegExp</code> <emu-xref href="#constructor" id="_ref_4752"><a href="#constructor">constructor</a></emu-xref>) that are allowed to throw <emu-val>SyntaxError</emu-val> are permitted to exhibit implementation-defined behaviour instead of throwing <emu-val>SyntaxError</emu-val> when they encounter an implementation-defined extension to the script syntax or regular expression pattern or flag syntax.
    
    </li>
    <li>
      Except as restricted in  <emu-xref href="#sec-forbidden-extensions" id="_ref_385"><a href="#sec-forbidden-extensions">16.2</a></emu-xref>,
 an implementation may provide additional types, values, objects, 
properties, and functions beyond those described in this specification. 
This may cause constructs (such as looking up a variable in the global 
scope) to have implementation-defined behaviour instead of throwing an 
error (such as <emu-val>ReferenceError</emu-val>).
    
    </li>
  </ul>

  <emu-clause id="sec-host-report-errors" aoid="HostReportErrors">
    <h1><span class="secnum">16.1</span>HostReportErrors ( <var>errorList</var> )</h1>

    <p>HostReportErrors is an implementation-defined abstract operation 
that allows host environments to report parsing errors, early errors, 
and runtime errors.</p>

    <p>An implementation of HostReportErrors must complete normally in 
all cases. The default implementation of HostReportErrors is to 
unconditionally return an empty normal completion.</p>

    <emu-note><span class="note">Note</span><div class="note-contents">
      <p><var>errorList</var> will be a <emu-xref href="#sec-list-and-record-specification-type" id="_ref_4753"><a href="#sec-list-and-record-specification-type">List</a></emu-xref> of ECMAScript language values. If the errors are parsing errors or early errors, these will always be <emu-val>SyntaxError</emu-val> or <emu-val>ReferenceError</emu-val> objects. Runtime errors, however, can be any ECMAScript value.</p>
    </div></emu-note>
  </emu-clause>

  <emu-clause id="sec-forbidden-extensions">
    <h1><span class="secnum">16.2</span>Forbidden Extensions</h1>
    <p>An implementation must not extend this specification in the following ways:</p>
    <ul>
      <li>
        ECMAScript function objects defined using syntactic constructors in <emu-xref href="#sec-strict-mode-code" id="_ref_4754"><a href="#sec-strict-mode-code">strict mode code</a></emu-xref> must not be created with own properties named <code>"caller"</code> or <code>"arguments"</code>. Such own properties also must not be created for function objects defined using an <emu-nt id="_ref_14272"><a href="#prod-ArrowFunction">ArrowFunction</a></emu-nt>, <emu-nt id="_ref_14273"><a href="#prod-MethodDefinition">MethodDefinition</a></emu-nt>, <emu-nt id="_ref_14274"><a href="#prod-GeneratorDeclaration">GeneratorDeclaration</a></emu-nt>, <emu-nt id="_ref_14275"><a href="#prod-GeneratorExpression">GeneratorExpression</a></emu-nt>, <emu-nt id="_ref_14276"><a href="#prod-AsyncGeneratorDeclaration">AsyncGeneratorDeclaration</a></emu-nt>, <emu-nt id="_ref_14277"><a href="#prod-AsyncGeneratorExpression">AsyncGeneratorExpression</a></emu-nt>, <emu-nt id="_ref_14278"><a href="#prod-ClassDeclaration">ClassDeclaration</a></emu-nt>, <emu-nt id="_ref_14279"><a href="#prod-ClassExpression">ClassExpression</a></emu-nt>, <emu-nt id="_ref_14280"><a href="#prod-AsyncFunctionDeclaration">AsyncFunctionDeclaration</a></emu-nt>, <emu-nt id="_ref_14281"><a href="#prod-AsyncFunctionExpression">AsyncFunctionExpression</a></emu-nt>, or <emu-nt id="_ref_14282"><a href="#prod-AsyncArrowFunction">AsyncArrowFunction</a></emu-nt> regardless of whether the definition is contained in <emu-xref href="#sec-strict-mode-code" id="_ref_4755"><a href="#sec-strict-mode-code">strict mode code</a></emu-xref>. Built-in functions, strict functions created using the <code>Function</code> <emu-xref href="#constructor" id="_ref_4756"><a href="#constructor">constructor</a></emu-xref>, generator functions created using the <code>Generator</code> <emu-xref href="#constructor" id="_ref_4757"><a href="#constructor">constructor</a></emu-xref>, async functions created using the <code>AsyncFunction</code> <emu-xref href="#constructor" id="_ref_4758"><a href="#constructor">constructor</a></emu-xref>, and functions created using the <code>bind</code> method also must not be created with such own properties.
      
      </li>
      <li>
        If an implementation extends any <emu-xref href="#function-object" id="_ref_4759"><a href="#function-object">function object</a></emu-xref> with an own property named <code>"caller"</code> the value of that property, as observed using [[Get]] or [[GetOwnProperty]], must not be a <emu-xref href="#strict-function" id="_ref_4760"><a href="#strict-function">strict function</a></emu-xref> object. If it is an <emu-xref href="#sec-object-type" id="_ref_4761"><a href="#sec-object-type">accessor property</a></emu-xref>, the function that is the value of the property's [[Get]] attribute must never return a <emu-xref href="#strict-function" id="_ref_4762"><a href="#strict-function">strict function</a></emu-xref> when called.
      
      </li>
      <li>
        Neither mapped nor unmapped arguments objects may be created with an own property named <code>"caller"</code>.
      
      </li>
      <li>
        The behaviour of the following methods must not be extended except as specified in ECMA-402: <code>Object.prototype.toLocaleString</code>, <code>Array.prototype.toLocaleString</code>, <code>Number.prototype.toLocaleString</code>, <code>Date.prototype.toLocaleDateString</code>, <code>Date.prototype.toLocaleString</code>, <code>Date.prototype.toLocaleTimeString</code>, <code>String.prototype.localeCompare</code>, <emu-xref href="#sec-%typedarray%-intrinsic-object" id="_ref_4763"><a href="#sec-%typedarray%-intrinsic-object">%TypedArray%</a></emu-xref><code>.prototype.toLocaleString</code>.
      
      </li>
      <li>
        The RegExp pattern grammars in  <emu-xref href="#sec-patterns" id="_ref_386"><a href="#sec-patterns">21.2.1</a></emu-xref> and  <emu-xref href="#sec-regular-expressions-patterns" id="_ref_387"><a href="#sec-regular-expressions-patterns">B.1.4</a></emu-xref> must not be extended to recognize any of the source characters A-Z or a-z as <emu-nt params="+U" id="_ref_14283"><a href="#prod-IdentityEscape">IdentityEscape</a><emu-mods><emu-params>[+U]</emu-params></emu-mods></emu-nt> when the  <sub>[U]</sub> grammar parameter is present.
      
      </li>
      <li>
        The Syntactic Grammar must not be extended in any manner that allows the token <code>:</code> to immediately follow source text that matches the <emu-nt id="_ref_14284"><a href="#prod-BindingIdentifier">BindingIdentifier</a></emu-nt> nonterminal symbol.
      
      </li>
      <li>
        When processing <emu-xref href="#sec-strict-mode-code" id="_ref_4764"><a href="#sec-strict-mode-code">strict mode code</a></emu-xref>, the syntax of <emu-nt id="_ref_14285"><a href="#prod-NumericLiteral">NumericLiteral</a></emu-nt> must not be extended to include  <emu-xref href="#prod-annexB-LegacyOctalIntegerLiteral" id="_ref_388"><a href="#prod-annexB-LegacyOctalIntegerLiteral"><emu-nt>LegacyOctalIntegerLiteral</emu-nt></a></emu-xref> and the syntax of <emu-nt id="_ref_14286"><a href="#prod-DecimalIntegerLiteral">DecimalIntegerLiteral</a></emu-nt> must not be extended to include  <emu-xref href="#prod-annexB-NonOctalDecimalIntegerLiteral" id="_ref_389"><a href="#prod-annexB-NonOctalDecimalIntegerLiteral"><emu-nt>NonOctalDecimalIntegerLiteral</emu-nt></a></emu-xref> as described in  <emu-xref href="#sec-additional-syntax-numeric-literals" id="_ref_390"><a href="#sec-additional-syntax-numeric-literals">B.1.1</a></emu-xref>.
      
      </li>
      <li>
        <emu-nt id="_ref_14287"><a href="#prod-TemplateCharacter">TemplateCharacter</a></emu-nt> must not be extended to include  <emu-xref href="#prod-annexB-LegacyOctalEscapeSequence" id="_ref_391"><a href="#prod-annexB-LegacyOctalEscapeSequence"><emu-nt>LegacyOctalEscapeSequence</emu-nt></a></emu-xref> as defined in  <emu-xref href="#sec-additional-syntax-string-literals" id="_ref_392"><a href="#sec-additional-syntax-string-literals">B.1.2</a></emu-xref>.
      
      </li>
      <li>
        When processing <emu-xref href="#sec-strict-mode-code" id="_ref_4765"><a href="#sec-strict-mode-code">strict mode code</a></emu-xref>, the extensions defined in  <emu-xref href="#sec-labelled-function-declarations" id="_ref_393"><a href="#sec-labelled-function-declarations">B.3.2</a></emu-xref>,  <emu-xref href="#sec-block-level-function-declarations-web-legacy-compatibility-semantics" id="_ref_394"><a href="#sec-block-level-function-declarations-web-legacy-compatibility-semantics">B.3.3</a></emu-xref>,  <emu-xref href="#sec-functiondeclarations-in-ifstatement-statement-clauses" id="_ref_395"><a href="#sec-functiondeclarations-in-ifstatement-statement-clauses">B.3.4</a></emu-xref>, and  <emu-xref href="#sec-initializers-in-forin-statement-heads" id="_ref_396"><a href="#sec-initializers-in-forin-statement-heads">B.3.6</a></emu-xref> must not be supported.
      
      </li>
      <li>
        When parsing for the <emu-nt id="_ref_14288"><a href="#prod-Module">Module</a></emu-nt> <emu-xref href="#sec-context-free-grammars" id="_ref_4766"><a href="#sec-context-free-grammars">goal symbol</a></emu-xref>, the lexical grammar extensions defined in  <emu-xref href="#sec-html-like-comments" id="_ref_397"><a href="#sec-html-like-comments">B.1.3</a></emu-xref> must not be supported.
      
      </li>
    </ul>
  </emu-clause>
</emu-clause>
