# ecmascript-2019

## What is this?
A reproduction of the latest [ECMAScript standard](https://www.ecma-international.org/ecma-262/10.0/index.html)(2019 edition as of writing).

## Why reproduce?
The spec is not easy to read on mobile devices and I read primarily on mobile. So I have tried to make it easier to read on mobile devices.

## Does it not violate copyright laws?
The thought had crossed my mind. So I looked up the [copyright section](https://www.ecma-international.org/ecma-262/10.0/index.html#sec-copyright-and-software-license) in the spec and found this:
>Copyright Notice

>© 2019 Ecma International

>This document may be copied, published and distributed to others, and certain derivative works of it may be prepared, copied, published, and distributed, in whole or in part, provided that the above copyright notice and this Copyright License and Disclaimer are included on all such copies and derivative works. The only derivative works that are permissible under this Copyright License and Disclaimer are:

>(i) ...

>(ii) works which incorporate all or portion of this document for the purpose of incorporating features that provide accessibility,

>(iii) ...

>(iv) ...

I trust that this repository is eligible to be published on the basis that it provides accessibility features. If it is not eligible to be published, my plea to the concerned person is let me know so that I can take it down. My email id:[karne.teja@gmail.com](mailto:karne.teja@gmail.com).
