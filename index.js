var fs = require('fs');

var names = ["intro", "scope", "conformance", "normative-references", "overview", "notational-conventions", "ecmascript-data-types-and-values", "abstract-operations", "executable-code-and-execution-contexts", "ordinary-and-exotic-objects-behaviours", "ecmascript-language-source-code", "ecmascript-language-lexical-grammar", "ecmascript-language-expressions", "ecmascript-language-statements-and-declarations", "ecmascript-language-functions-and-classes", "ecmascript-language-scripts-and-modules", "error-handling-and-language-extensions", "ecmascript-standard-built-in-objects", "global-object", "fundamental-objects", "numbers-and-dates", "text-processing", "indexed-collections", "keyed-collections", "structured-data", "control-abstraction-objects", "reflection", "memory-model", "grammar-summary", "additional-ecmascript-features-for-web-browsers", "strict-mode-of-ecmascript", "corrections-and-clarifications-in-ecmascript-2015-with-possible-compatibility-impact", "additions-and-changes-that-introduce-incompatibilities-with-prior-editions", "colophon", "bibliography", "copyright-and-software-license"];

var noOfFiles = names.length;

function generateHTML(data,index){
  var str = `<!doctype html>
  <html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=IBM+Plex+Mono:400,400i,700,700i&display=swap" rel="stylesheet">
    <link href="../css/ecmarkup.css" rel="stylesheet">
    <title>${names[index]}</title>
  </head>
  <body>
    ${data}
    <nav class="bottom-nav">
      <ul>
        <li><a href="${names[index-1]}.html">Previous</a></li>
        <li><a href="toc.html">TOC</a></li>
        <li><a href="${names[index+1]}.html">Next</a></li>
      </ul>
    </nav>
    <script src="../js/script.js"></script>
  </body>
  </html>`;

  return str;
}

function generateHTMLFiles(){
  names.forEach(function(item,index){
    fs.readFile(`html/${item}.html`, 'utf8', (err, data) => {
      var str = generateHTML(data,index);
      fs.appendFile(`dist/${item}.html`, str, (err) => {if (err) throw err;});
    });
  });
}

function createFiles(folder){
  names.forEach(function(item){
    fs.appendFile(`${folder}/${item}.html`, "", ()=>{console.log('I am here as a filler')});
  });
}

//createFiles('dist'); // If you want the files created in 'html' folder, change the argument to 'html'
generateHTMLFiles();
